package com.android.sample.animatedscroller;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.widget.ScrollView;

/**
 * Created by Yousuf Syed.
 */
public class ParallaxScrollView extends ScrollView {

    private OnScrollListener mOnScrollChangedCallback;

    public ParallaxScrollView(final Context context) {
        super(context);
    }

    public ParallaxScrollView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ParallaxScrollView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedCallback != null) {
            mOnScrollChangedCallback.onScroll(l, t, oldl, oldt);
        }
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        if(mOnScrollChangedCallback != null){
            mOnScrollChangedCallback.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        }
    }

    public void setOnScrollChangedListener(OnScrollListener onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Interface to implement by object interested in listener, interested in scroll change on scrollview.
     **/
    public interface OnScrollListener {
        void onScroll(int left, int top, int oldLeft, int oldTop);
        void onOverScrolled (int scrollX, int scrollY, boolean clampedX, boolean clampedY);
    }
}
