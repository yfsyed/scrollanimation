package com.android.sample.animatedscroller;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
//import org.apache.commons.lang.SerializationUtils;

/**
 * Created by u471637 on 1/6/16.
 */
public class AnimatingContainer extends FrameLayout implements ParallaxScrollView.OnScrollListener {

    private static final String TAG = AnimatingContainer.class.getSimpleName();

    private int mActionHeaderId;
    private int mWebViewId;
    private int mStickyHeaderId;

    private View mBackgroundContainer;
    private ViewGroup mStickyHeader;
    private ViewGroup mStickyHeaderContainer;
    private ParallaxScrollView mCustomScrollView;

    private static final int ANIMATION_DURATION = 175;
    private static final int CONCURRENT_ANIMATION_DURATION = 0;

    private static float mRelativeStartYPosition = 0;
    private static float mDefaultOverlayHeight = 300;


    public AnimatingContainer(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AnimatingContainer(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray viewAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.ResizableLayout);
        mWebViewId = viewAttributes.getResourceId(R.styleable.ResizableLayout_scrollview_id, 0);
        mActionHeaderId = viewAttributes.getResourceId(R.styleable.ResizableLayout_background_view_id, 0);
        mStickyHeaderId = viewAttributes.getResourceId(R.styleable.ResizableLayout_sticky_header_id, 0);

        mDefaultOverlayHeight = viewAttributes.getDimension(R.styleable.ResizableLayout_overlayHeight, 300);
        viewAttributes.recycle();

        if (mActionHeaderId == 0 || mWebViewId == 0 || mStickyHeaderId == 0) {
            throw new IllegalArgumentException(viewAttributes.getPositionDescription() + ": The required attribute must refer to a valid child view.");
        }
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        try {
            initAnimatingWebView();
        } catch (Exception e) {
            throw new RuntimeException("Your Panel must have a child View");
        }
    }

    private void initAnimatingWebView() {
        mBackgroundContainer = findViewById(mActionHeaderId);
        mCustomScrollView = (ParallaxScrollView) findViewById(mWebViewId);
        mStickyHeader = (ViewGroup) findViewById(mStickyHeaderId);

        mStickyHeader.animate().alpha(0).setDuration(0).start();
        mStickyHeader.setElevation(5);

        mCustomScrollView.setFocusable(true);
        mCustomScrollView.setFocusableInTouchMode(true);
        mCustomScrollView.requestFocus(View.FOCUS_DOWN);
        mCustomScrollView.setOnScrollChangedListener(this);
        Log.v(TAG, "xScale" + mBackgroundContainer.getScaleX() + " yScale" + mBackgroundContainer.getScaleX());

    }

    @Override
    public void onScroll(int l, int top, int oldl, int oldt) {
        mRelativeStartYPosition = mCustomScrollView.getScrollY();
        animateView(mRelativeStartYPosition, CONCURRENT_ANIMATION_DURATION);
        Log.v(TAG, "Scroll delta: " + mRelativeStartYPosition);
    }

    private void animateView(float delta, int duration) {

        if (delta >= mDefaultOverlayHeight) {
            mStickyHeader.animate().alpha(1).setDuration(0);
        } else {
            mStickyHeader.animate().alpha(0).setDuration(0);
        }

        delta *= .40;
        mBackgroundContainer.animate().alpha(animateActionHeader(Math.abs(delta)))
                .y(-1 * delta).setInterpolator(new LinearOutSlowInInterpolator())
                .setDuration(duration).start();
    }

    private float animateActionHeader(float delta) {
        float height = mBackgroundContainer.getHeight();
        float alpha = 1;

        if (delta <= height) {
            alpha = 1 - (delta / height);
        }
        return alpha;
    }


    private static boolean overScroll;

    @Override
    public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {

        synchronized (animationListener) {
            if (mBackgroundContainer.getY() == 0 && !overScroll) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.scale_in_out);
                animation.setAnimationListener(animationListener);
                mBackgroundContainer.startAnimation(animation);
            }
        }
    }

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
            overScroll = true;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            Log.v(TAG, "xScale" + mBackgroundContainer.getScaleX() +" yScale" + mBackgroundContainer.getScaleX());
            overScroll = false;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
}
